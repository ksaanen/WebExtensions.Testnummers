# WebExtensions.Testnummers

Testnummers is a Dutch addon to be used in Chrome, Firefox or Edge for generating numbers or strings when testing input flows. With a simple right click and hit on one of the options it'll generate a number or string in the selected input field.

Currently Testnummers generates:
- BSN
- IBAN (static string)
- Kenteken (license plate)
- Date of birth (< 18)
- Date of birth (> 18)

Version history:
- v 0.4: simplified randomInt & added randomValue (array) function.
- v 0.3: fixed date formatting for date-of-birth entries.