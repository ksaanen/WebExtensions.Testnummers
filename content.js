(function(){
  "use strict";

  var domObject; // save selected DOM element

  document.addEventListener('mousedown', function(event){
    if (event.target.tagName === 'INPUT') {
      domObject = event.target;
    }
  });

  chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {
      if (request.execute === 'getBSN')     { domObject.value = genBSN() };
      if (request.execute === 'getIBAN')    { domObject.value = genIBAN() };
      if (request.execute === 'getOnder18') { domObject.value = genOnder18() };
      if (request.execute === 'getBoven18') { domObject.value = genBoven18() };
      if (request.execute === 'getKenteken') { domObject.value = genKenteken() };
    }
  );

  function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
  }

  function getRandomValue(array) {
    return array[getRandomInt(0,(array.length-1))];
  }

  function dateFormat(n) {
    return (n < 10 ? '0' : '') + n;
  }

  function genOnder18() {
    var d = new Date(),
        day = (d.getDate() + 1) - getRandomInt(0, d.getDate()),
        month = (d.getMonth() + 1) - getRandomInt(0, d.getMonth()),
        year = d.getFullYear() - getRandomInt(0, 18);
        
    return (dateFormat(day) + '-' + dateFormat(month) + '-' + year);
  }

  function genBoven18() {
    var d = new Date(),
        day = getRandomInt(1, 28),
        month = getRandomInt(1, 12),
        year = d.getFullYear() - getRandomInt(19, 30);

    return (dateFormat(day) + '-' + dateFormat(month) + '-' + year);
  }

  function genIBAN() {
    return 'NL13TEST123456789';
  }

  function genBSN() {
    // Generate random BSN number
    var returnValue='',
        SofiNr=0,
        Nr9=Math.floor(Math.random()*7),
        Nr8=Math.floor(Math.random()*10),
        Nr7=Math.floor(Math.random()*10),
        Nr6=Math.floor(Math.random()*10),
        Nr5=Math.floor(Math.random()*10),
        Nr4=Math.floor(Math.random()*10),
        Nr3=Math.floor(Math.random()*10),
        Nr2=Math.floor(Math.random()*10),
        Nr1=0;

    if((Nr9==0)&&(Nr8==0)&&(Nr7==0)) {
      Nr8=1;
    }
    SofiNr=9*Nr9+8*Nr8+7*Nr7+6*Nr6+5*Nr5+4*Nr4+3*Nr3+2*Nr2;
    Nr1=Math.floor(SofiNr-(Math.floor(SofiNr/11))*11);

    if(Nr1>9) {
      if(Nr2>0) {
        Nr2-=1;Nr1=8;
      } else {
        Nr2+=1;Nr1=1;
      }
    }

    returnValue+=Nr9;
    returnValue+=Nr8;
    returnValue+=Nr7;
    returnValue+=Nr6;
    returnValue+=Nr5;
    returnValue+=Nr4;
    returnValue+=Nr3;
    returnValue+=Nr2;
    returnValue+=Nr1;
    return returnValue;
  }

  function genKenteken() {
    var characters = 'GHJLNPRSTXZ',
        patterns = [
          'XX-99-99',
          '99-99-XX',
          '99-XX-99',
          'XX-99-XX',
          'XX-XX-99',
          '99-XX-XX',
          '99-XXX-9',
          '9-XXX-99',
          'XX-999-X',
          //'X-999-XX', // pattern has a very specific set of characters
          'XXX-99-X',
          'X-99-XXX',
          '9-XX-999',
          '999-XX-9'],
        pattern = getRandomValue(patterns),
        kenteken = '';

    for (var i = 0; i < pattern.length; i++) {
      var c = new RegExp(pattern[i]);
      if (pattern[i] === '-') {
        kenteken += '-';
      } else if (c.test("[^0-9]") === true) {
        kenteken += getRandomInt(0,9);
      } else {
        kenteken += characters.charAt(getRandomInt(0,10));
      }
    }

    return kenteken;
  }

})();


