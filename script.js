(function(){
  "use strict";
    
  chrome.contextMenus.create({
    id: "getBSN",
    title: "BSN nummer",
    contexts: ["editable"],
    onclick: query
  });

  chrome.contextMenus.create({
    id: "getIBAN",
    title: "IBAN nummer",
    contexts: ["editable"],
    onclick: query
  });

  chrome.contextMenus.create({
    id: "clickme-separator-1",
    type: "separator",
    contexts: ["editable"]
  });

  chrome.contextMenus.create({
    id: "getKenteken",
    title: "Kenteken",
    contexts: ["editable"],
    onclick: query
  });

  chrome.contextMenus.create({
    id: "clickme-separator-2",
    type: "separator",
    contexts: ["editable"]
  });

  chrome.contextMenus.create({
    id: "getOnder18",
    title: "Geboortedatum onder 18",
    contexts: ["editable"],
    onclick: query
  });

  chrome.contextMenus.create({
    id: "getBoven18",
    title: "Geboortedatum boven 18",
    contexts: ["editable"],
    onclick: query
  });

  function query(info, tab){
    chrome.tabs.query({active: true, currentWindow: true}, function(tab) {
      chrome.tabs.sendMessage(tab[0].id, {execute: info.menuItemId});
    });
  }

})();